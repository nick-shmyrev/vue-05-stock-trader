import Vue from 'vue';
import App from './App.vue';

import store from './store/store.js';

import VueRouter from 'vue-router';
import {routes} from './routes.js';
Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: 'history'
});

import VueResource from 'vue-resource';
Vue.use(VueResource);
Vue.http.options.root = 'https://vue-stock-trader-22b8a.firebaseio.com/';

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
});
