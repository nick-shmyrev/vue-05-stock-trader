import stocks from './../../data/stocks.js';

const state = {
  stocks: []
};
const getters = {
  stocks(state) {
    return state.stocks;
  },
};
const mutations = {
  'stocks/SET_STOCKS'(state, stocks) {
    state.stocks = stocks;
  },
  'stocks/RND_STOCKS'(state) {
    state.stocks.forEach((stock) => {
      stock.price += Math.round(stock.price * (.5 - Math.random())/6);
    });
  },
};
const actions = {
  buyStock(context, order) {
    context.commit('portfolio/BUY_STOCK', order);
  },
  initStocks(context) {
    context.commit('stocks/SET_STOCKS', stocks);
  },
  randomizeStocks(context) {
    context.commit('stocks/RND_STOCKS');
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};