const state = {
  funds: 20000,
  stocks: [],
};

const getters = {
  findRecord: (state) => (order) => {
    return state.stocks.find((el) => {
      return el.id === order.id
    });
  },
  stockPortfolio(state, getters) {
    return state.stocks.map((stock) => {
      const record = getters.stocks.find((el) => {
        return el.id === stock.id;
      });
      
      return {
        id: stock.id,
        quantity: stock.quantity,
        name: record.name,
        price: record.price
      };
    });
  },
  funds(state) {
    return state.funds;
  },
};

const mutations = {
  'portfolio/BUY_STOCK'(state, order) {
    // order = {id, name, price, quantity}
  
    const record = this.getters.findRecord(order);
    const orderTotal = order.price * order.quantity;
    
    if (orderTotal > state.funds) {
      return;
    }
    
    if (record) {
      record.quantity += order.quantity;
    } else {
      state.stocks.push({
        id: order.id,
        quantity: order.quantity,
      });
    }
    state.funds -= orderTotal;
  },
  'portfolio/SELL_STOCK'(state, order) {
    // order = {id, name, price, quantity}
    
    const record = this.getters.findRecord(order);
    const orderTotal = order.price * order.quantity;
  
    if (order.quantity > record.quantity) {
      return;
    }
    
    if (record.quantity > order.quantity) {
      record.quantity -= order.quantity;
    } else {
      state.stocks.splice(state.stocks.indexOf(record), 1);
    }
    state.funds += orderTotal;
  },
  'portfolio/SET_PORTFOLIO'(state, data) {
    state.funds = data.funds;
    state.stocks = data.stockPortfolio ? data.stockPortfolio : [];
  },
};

const actions = {
  sellStock(context, order) {
    context.commit('portfolio/SELL_STOCK', order);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};