import Vue from 'vue';

export const loadData = (context) => {
  Vue.http.get('data.json')
    .then((res) => {
      console.log(res.data);
      if (res.data) {
        const {stocks, stockPortfolio, funds} = res.data;
  
        context.commit('stocks/SET_STOCKS', stocks);
        context.commit('portfolio/SET_PORTFOLIO', {stockPortfolio, funds});
      }
    })
  .catch(err => console.log(err));
};